
                          Shoot me if you can

  What is it?
  -----------

  'Shoot me if you can' is a 2-Dimensional multiplayer game that is being developed as a project in the cs243 course in the spring semester of 2016 at IITG.It is being done by four b-tech 2nd year students.

  The Latest Version
  ------------------

  the latest version is on the gitlab repository but it is not it.
it is only a basic version of our game and new features will keep rolling out.

  Documentation
  -------------

  the doucumentation can be found from the 'documentation' file on the repository.

  Installation
  ------------

  the final game will be played on linux but at present the build file is a html file and can be played on mozilla firefox.

  Licensing
  ---------

  Please see the file called LICENSE.

  Contacts
  --------

  contact : h.vamsi@iitg.ernet.in for further information.
