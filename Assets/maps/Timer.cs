﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

	public class RoomController: MonoBehaviour {

	float timeLeft = 30.0f;
	public Text time;
	// Use this for initialization
	void Start () {
		time.text = "Time left: " + timeLeft.ToString();
	}

	
	void Update()
	{
		while (timeLeft>=0) 
		{
			timeLeft -= Time.deltaTime;
			time.text = "Time left: " + timeLeft.ToString ();
		}
		time.text = "Time up";
			}


	// Update is called once per frame

}
