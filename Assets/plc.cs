﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class plc : MonoBehaviour
{
	public char m_PlayerNumber;              // Used to identify which player belongs to which player.  This is set by this player's manager.
	public float m_Speed = 12f;                 // How fast the player moves forward and back.
	public float m_TurnSpeed = 180f;            // How fast the player turns in degrees per second.
	public float m_radius=5f;
	public LayerMask m_layermask;

	private string m_MovementAxisName;          // The name of the input axis for moving forward and back.
	private string m_TurnAxisName;              // The name of the input axis for turning.
	private Rigidbody m_Rigidbody;              // Reference used to move the player.
	private float m_MovementInputValue;         // The current value of the movement input.
	private float m_TurnInputValue;             // The current value of the turn input.
	private bool trapped = false;
	//for testing:
	public bool b1 = false;
	public bool b2 = false;
	public bool b3 = false;
	public bool b4 = false;
	public GameObject myObject;
	public Material myMaterial;
	private Vector3[] vert = new Vector3[4];
	public int numbull;
	private void Awake ()
	{
		m_Rigidbody = GetComponent<Rigidbody> ();
	}


	private void OnEnable ()
	{
		// When the player is turned on, make sure it's not kinematic.
		m_Rigidbody.isKinematic = false;

		// Also reset the input values.
		m_MovementInputValue = 0f;
		m_TurnInputValue = 0f;
	}


	private void OnDisable ()
	{
		// When the player is turned off, set it to kinematic so it stops moving.
		m_Rigidbody.isKinematic = true;
	}


	private void Start ()
	{
		// The axes names are based on player number.
		m_PlayerNumber = gameObject.name[gameObject.name.Length - 1];
		m_MovementAxisName = "Vertical" + m_PlayerNumber;
		m_TurnAxisName = "Horizontal" + m_PlayerNumber;

	}


	private void Update ()
	{
		// Store the value of both input axes.
		m_MovementInputValue = Input.GetAxis (m_MovementAxisName);
		m_TurnInputValue = Input.GetAxis (m_TurnAxisName);
		Freeze();
	}


	private void Freeze()
	{
		Collider[] FrozenBullets = Physics.OverlapSphere(transform.position, 100, 1 << 9);
		int i, j, k, l,n = FrozenBullets.Length;
		numbull = n;
		Vector3[] Side;
		Vector3[] relPlayer;
		//vert = new Vector3[4];
		relPlayer = new Vector3[4];
		Side = new Vector3[4];
		if (n > 3)
		{
			for (i = 0; i < n - 3; i++)
			{
				for (j=i+1;j<n - 2; j++)
				{
					for (k = j + 1; k < n - 1; k++)
					{
						for (l = k + 1;l < n; l++)
						{
							Side[0] = FrozenBullets[i].transform.position - FrozenBullets[j].transform.position;
							Side[1] = FrozenBullets[j].transform.position - FrozenBullets[k].transform.position;
							Side[2] = FrozenBullets[k].transform.position - FrozenBullets[l].transform.position;
							Side[3] = FrozenBullets[l].transform.position - FrozenBullets[i].transform.position;
							relPlayer[0] = transform.position - FrozenBullets[j].transform.position;
							relPlayer[1] = transform.position - FrozenBullets[k].transform.position;
							relPlayer[2] = transform.position - FrozenBullets[l].transform.position;
							relPlayer[3] = transform.position - FrozenBullets[i].transform.position;
							vert[0] =FrozenBullets[i].transform.position;
							vert[1] =FrozenBullets[j].transform.position;
							vert[2] =FrozenBullets[k].transform.position;
							vert[3] =FrozenBullets 	[l].transform.position;
							if (b1 = CheckInAngle(Side[0],Side[1],relPlayer[0]) && (b2 = CheckInAngle(Side[1], Side[2], relPlayer[1])) && (b3 = CheckInAngle(Side[2], Side[3], relPlayer[2])) && ( b4 = CheckInAngle(Side[3], Side[0], relPlayer[3])))
							{
								//  trapped = true;
								Destroy(gameObject);

								//if (m_PlayerNumber == '1') {
								//	SceneManager.LoadScene ("sc2");
								//}
							}
						}
					}
				}
			}
		}
	}
	private Mesh CreateMesh()
	{
		Mesh mesh = new Mesh();
		Vector2[] uvs = new Vector2[vert.Length];
		for (int i=0; i < uvs.Length; i++) {
			uvs[i] = new Vector2(vert[i].x, vert[i].z);
		}
		mesh.uv = uvs;
		mesh.vertices = vert;
		return mesh;
	}

	private bool CheckInAngle(Vector3 Side1, Vector3 Side2, Vector3 relPlayer)
	{
		if (Vector3.Cross(Side1, relPlayer).y * Vector3.Cross(relPlayer, Side2).y < 0)
			return true;
		else
			return false;
	}

	private void FixedUpdate ()
	{
		// Adjust the rigidbodies position and orientation in FixedUpdate.
		if (!trapped)
		{
			Move();
			Turn();
		}
	}


	private void Move ()
	{
		// Create a vector in the direction the player is facing with a magnitude based on the input, speed and the time between frames.
		Vector3 movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;

		// Apply this movement to the rigidbody's position.
		m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
	}


	private void Turn ()
	{
		// Determine the number of degrees to be turned based on the input, speed and time between frames.
		float turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;

		// Make this into a rotation in the y axis.
		Quaternion turnRotation = Quaternion.Euler (0f, turn, 0f);

		// Apply this rotation to the rigidbody's rotation.
		m_Rigidbody.MoveRotation (m_Rigidbody.rotation * turnRotation);
	}
	private IEnumerator OnTriggerEnter (Collider other)
	{
		if (other.gameObject.CompareTag ("vamsi")) {
			Destroy (other.gameObject);
		} else if (other.gameObject.CompareTag ("geetu")) {
			Destroy (gameObject);
			Destroy(other.gameObject);
			SceneManager.LoadScene ("sc2");
		} else if (other.gameObject.CompareTag ("Attacker"))
		{
			Destroy (other.gameObject);
			yield return new WaitForSeconds(3);
			if (m_PlayerNumber == '1') {
				SceneManager.LoadScene ("sc2");
			}

		}
	}
}