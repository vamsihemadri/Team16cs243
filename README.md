                          Project Sampoo

  What is it?
  -----------

  Project Sampoo is a 2-Dimensional multiplayer game that is being developed as a project in the cs243 course in the spring semester of 2016 at IITG.It is being done by four b-tech 2nd year students.

  The Latest Version
  ------------------

  The latest stable version is on gitlab. However we are constantly rolling out new features and updating it in this repository.

  Documentation
  -------------

  The documentation can be found from the 'documentation' file on the repository.

  Installation
  ------------

  The final game will be played on windows and most probably linux as well, but at present the build file is a html file and can be played on mozilla firefox.

  Licensing
  ---------

  Please see the file called LICENSE.

  Dependencies
  ------------
  This game is being developed using:
    Unity Game Development Engine under UNITY PERSONAL SOFTWARE LICENSE AGREEMENT 5.X.
    System Library Package for C#.
    Package made freely available by MrNinjaBoy (Youtube account) to his Youtube subscribers  (for textures)
  
  Contacts
  --------
  contact : h.vamsi@iitg.ernet.in for further information.